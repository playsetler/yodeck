<?php
use App\Http\Controllers\Api\AuthController;
use Illuminate\Support\Facades\Route;

/* Route::get('{any}', function () {
    return view('app');
})->where('any', '.*'); */

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
/* if(Auth::check()) {
    Route::get('{page}', 'MainController')->where(name: 'page', expression: '.*');
} else {
    Route::get('{page}', function () { return redirect()->route('/auth/login'); });
} */
Route::get('/preview/{layout}/', [App\Http\Controllers\Layout\ShowLayoutController::class, 'get']);
Route::get('{catchall}', [App\Http\Controllers\HomeController::class, 'index'])->where('catchall', '.*');



//Route::get('/{page?}', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
