require("./bootstrap");
window.Vue = require("vue").default;
import { createApp } from "vue";
import router from "./router";

const app = createApp({});
app.config.unwrapInjectedRef = true;
app.component('Layouts', require('./pages/Layouts.vue').default);
app.use(router).mount("#app");