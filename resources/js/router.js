import { createRouter, createWebHistory } from "vue-router";
import Login from "./pages/Login";
import Registration from "./pages/Registration";
import Layouts from "./pages/Layouts";
import Layout from "./pages/Layout";
import Profile from "./pages/Profile";
import Test from "./pages/Test";

const routes = [
    {
        path:"/",
        name: "Layouts",
        component: Layouts,
        alias: ['/index.html', '/home', '/layouts'],
        meta: { requiresAuth: true }
    },
    {
        path:"/login",
        name: "Login",
        component: Login
    },
    {
        path:"/register",
        name: "Registration",
        component: Registration
    },
    {
        path:"/layouts/:id",
        name: "Layout",
        component: Layout
    },
    {
        path:"/profile",
        name: "Profile",
        component: Profile
    },
    {
        path:"/create",
        name: "Create",
        component: Layout,
        meta: { requiresAuth: true }
    },
    {
        path:"/edit/:id",
        name: "Edit",
        component: Layout,
        meta: { requiresAuth: true }
    },
    {
        path:"/test",
        name: "Test",
        component: Test
    }];

const router = createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
});

/* router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('jwt') == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            });
        } else {
            let user = JSON.parse(localStorage.getItem('user'))
            if(to.matched.some(record => record.meta.is_admin)) {
                if(user.is_admin == 1) {
                    next();
                }
                else{
                    next({ name: 'userboard'});
                }
            }else {
                next();
            }
        }
    } else if(to.matched.some(record => record.meta.guest)) {
        if(localStorage.getItem('jwt') == null) {
            next();
        } else {
            next({ name: 'userboard'});
        }
    } else {
        next();
    }
}); */

export default router