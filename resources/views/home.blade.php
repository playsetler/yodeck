@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- <link rel="stylesheet" href="{{ asset('css/panel.css') }}">
<script src="{{ asset('js/panel.js') }}" defer></script>
<div class="container-md">
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container-md">
            <a class="navbar-brand" href="#"><img src="{{ asset('img/brand.png') }}" class="img-fluid">{{ config('app.name', 'Laravel') }}</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbarLabel">{{ __('Menu') }}</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link" href="layouts.html">{{ __('Layouts') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">{{ __('Media') }}</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {{ __('Account') }}
                            </a>
                        <ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
                            <li><a class="dropdown-item" href="#"><i class="bi bi-person-circle"></i>{{ __('Profile') }}</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-gear"></i>{{ __('Settings') }}</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{ route('logout') }}"><i class="bi bi-box-arrow-right"></i>{{ __('Logout') }}</a></li>
                        </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <main class="row g-0">
        <h5>{{ __('All layouts') }}</h5>
        <table class="table table-borderless">
            <thead>
                <tr>
                    <td scope="col" class="table-col-chekbox">
                        <div class="table-select">
                            <input class="form-check-input" type="checkbox" id="select-all">
                            <label class="form-check-label" for="select-all"></label>
                        </div>
                    </td>
                    <th scope="col"><div class="table-name">{{ __('NAME') }}</div></th>
                    <th scope="col"><div class="table-time">{{ __('MODIFIED') }}</div></th>
                    <th scope="col"><div class="table-action">{{ __('ACTIONS') }}</div></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="row" class="table-col-chekbox">
                        <div class="table-select">
                            <input class="form-check-input" type="checkbox" id="select-1">
                            <label class="form-check-label" for="select-1"></label>
                        </div>
                    </td>
                    <td><div class="table-name"><a href="#">Simple Layout</a></div></td>
                    <td><div class="table-time">Feb 2nd, 2022 - 04:04</div></td>
                    <td class="td-action">
                        <div class="table-action">
                            <a href="#" class="actions-container">
                            <i class="bi bi-three-dots"></i>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="edit dropdown-item"><i class="bi bi-pencil-square"></i>{{ __('Edit') }}</a></li>
                                <li><a href="#" class="delete dropdown-item"><i class="bi bi-trash"></i>{{ __('Delete') }}</a></li>
                                <li></li>
                            </ul>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td scope="row" class="table-col-chekbox">
                        <div class="table-select">
                            <input class="form-check-input" type="checkbox" id="select-2">
                            <label class="form-check-label" for="select-2"></label>
                        </div>
                    </td>
                    <td><div class="table-name"><a href="#">FullScreen Layout</a></div></td>
                    <td><div class="table-time">Jan 7th, 2022 - 08:01</div></td>
                    <td class="td-action">
                        <div class="table-action">
                            <a href="#" class="actions-container">
                            <i class="bi bi-three-dots"></i>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="edit dropdown-item"><i class="bi bi-pencil-square"></i>{{ __('Edit') }}</a></li>
                                <li><a href="#" class="delete dropdown-item"><i class="bi bi-trash"></i>{{ __('Delete') }}</a></li>
                                <li></li>
                            </ul>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td scope="row" class="table-col-chekbox">
                        <div class="table-select">
                            <input class="form-check-input" type="checkbox" id="select-3">
                            <label class="form-check-label" for="select-3"></label>
                        </div>
                    </td>
                    <td><div class="table-name"><a href="#">We Are Closed</a></div></td>
                    <td><div class="table-time">Jan 7th, 2022 - 08:01</div></td>
                    <td class="td-action">
                        <div class="table-action">
                            <a href="#" class="actions-container">
                            <i class="bi bi-three-dots"></i>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="edit dropdown-item"><i class="bi bi-pencil-square"></i>{{ __('Edit') }}</a></li>
                                <li><a href="#" class="delete dropdown-item"><i class="bi bi-trash"></i>{{ __('Delete') }}</a></li>
                                <li></li>
                            </ul>
                            </a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <div class="row g-0">
            <div class="col">
                <button class="btn btn-primary mb-3">{{ __('Add Layout') }}</button>
                <button class="btn btn-secondary mb-3">{{ __('Actions') }}</button>
            </div>
        </div>
    </main>
</div> --> 
<div class="container-md">
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container-md">
            <a class="navbar-brand" href="#"><img src="img/brand.png" class="img-fluid">{{ config('app.name', 'Laravel') }}</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link" href="/layouts">Layouts</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Media</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Account
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
                            <li><a class="dropdown-item" href="#"><i class="bi bi-person-circle"></i>Profile</a></li>
                            <li><a class="dropdown-item" href="#"><i class="bi bi-gear"></i>Settings</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i class="bi bi-box-arrow-right"></i>
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <router-view :key="$route.path"></router-view>
</div>
@endsection